# README #

This is a python3 django application, that maps valid addresses onto a Google Fusion Table.

### How do I get set up? ###

```bash
git clone https://nuclearjoker@bitbucket.org/nuclearjoker/mappr.git
virtualenv -p /usr/bin/python3 ~/mappr_virtualenv
source ~/mappr_virtualenv/bin/activate
cd mappr
pip install -r requirements.txt
python manage.py migrate 
# then runserver.
python manage.py runserver
```
### How do I create a user? ###

```bash
python manage.py createsuperuser
```
from django.db import models


class Location(models.Model):

    address = models.TextField(max_length=1024, default="")
    latitude = models.FloatField(default=0.0)
    longitude = models.FloatField(default=0.0)

# Import the render shortcut method to render a template

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError


from django.shortcuts import render
from django.template import RequestContext
from django.conf import settings

from locations.services import LocationServices


# This is just to host the simple static viewset contain the map view.
def index(request):
    return render(
        request=request,
        template_name="index.html",
        context_instance=RequestContext(request, {
            "GOOGLE_BROWSER_KEY": settings.GOOGLE_BROWSER_KEY,
            "FUSION_TABLE_ID":    settings.FUSION_TABLE_ID
            })
        )


# api rest viewset.
class LocationViewset(viewsets.ViewSet):

    def create(self, request):
        created = False
        location = "Nothing created."

        if all([request.data.get('latitude'),
                request.data.get('longitude'),
                request.data.get('address_line')]):

            created, location = LocationServices.create(
                request.data['latitude'],
                request.data['longitude'],
                request.data['address_line'],
                add_to_fusion_table=True
                )

        if created:
            return Response({'status': 'OK'})
        else:
            raise ValidationError({'message': location})

    def delete(self, request, pk=None):

        if pk is None:  # this is a delete all.
            LocationServices.flush_all()

        return Response({'status': 'OK'})

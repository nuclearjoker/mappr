from django.conf import settings

from locations.models import Location
from locations.providers.google_location_services import GoogleLocationServices


class LocationQueries:

    @staticmethod
    def address_exists(address_line):
        res = Location.objects.filter(address=address_line).count()

        return res > 0


class LocationServices:

    @classmethod
    def create(cls, lat, lon, address_line="", validate_address=False, add_to_fusion_table=False):
        """ Create a location reference point, validate if necessary. """

        # validate coordinates if required, and override address line.
        if validate_address:
            is_address, address_line = cls.validate_address(lat, lon)
            if not is_address:
                return False, "Invalid address."

        # check if it exists already.
        if LocationQueries.address_exists(address_line):
            return False, "Address already added."

        location = Location(
            latitude=lat,
            longitude=lon,
            address=address_line
            )
        location.save()

        if add_to_fusion_table:
            GoogleLocationServices.insert_location_row(address_line, lat, lon)

        return True, location

    @staticmethod
    def flush_all(flush_fusion_table=True):
        Location.objects.all().delete()

        if flush_fusion_table:
            GoogleLocationServices.flush_all()


from httplib2 import Http

from geopy import GoogleV3
from oauth2client.service_account import ServiceAccountCredentials
from apiclient.discovery import build

from django.conf import settings


class GoogleLocationServices:
    """
    This is a utility service layer provides function to our
    share Fusion Table and geocoding functionality.
    """

    @staticmethod
    def _get_fusion_table_service():
        scopes = ['https://www.googleapis.com/auth/fusiontables']

        credentials = ServiceAccountCredentials.from_json_keyfile_name(
            'mappr/service_account.json',
            scopes=scopes
            )

        http = Http()
        http = credentials.authorize(http)

        service = build("fusiontables", "v1", http=http)

        return service

    @classmethod
    def insert_location_row(cls, address_line, lat, lon):
        """ INSERT an location into the fusion table """

        # print(service.query().sql(sql='SELECT * FROM 1gvB3SedL89vG5r1128nUN5ICyyw7Wio5g1w1mbk').execute())

        service = cls._get_fusion_table_service()

        sql_insert = """
            INSERT INTO {0} ('Address Line', 'Latitude', 'Longitude')
            VALUES ('{1}', '{2}', '{3}')
            """.format(
                    settings.FUSION_TABLE_ID,
                    address_line,
                    lat,
                    lon
                )

        res = service.query().sql(sql=sql_insert).execute()

        print(res)

        return res

    @classmethod
    def flush_all(cls):
        """ Remove all rows from fusion table. """

        service = cls._get_fusion_table_service()
        sql_delete = "DELETE FROM {0}".format(settings.FUSION_TABLE_ID)
        res = service.query().sql(sql=sql_delete).execute()

        return res

    @staticmethod
    def validate_address(lat, lon):
        """
        Lookup a latitude and longitude and see wether it a valid 'ROOFTOP' location type.
        """

        geolocator = GoogleV3(api_key=settings.GOOGLE_GEOCODE_KEY)

        res = geolocator.reverse("{0}, {1}".format(lat, lon))

        if res:

            address1 = res[0]
            print(res)
            print(address1.raw)

            if address1.raw['geometry']['location_type'] == "ROOFTOP":
                return True, address1.address

        return False, ""

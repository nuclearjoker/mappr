from django.contrib.auth.decorators import login_required
from django.conf.urls import url, include
from rest_framework import routers

from locations.views import index, LocationViewset

router = routers.SimpleRouter()
router.register('locations', LocationViewset, 'locations')

urlpatterns = [
    url(r'api/', include(router.urls)),
    url(r'^', login_required(index)),
]

from django.test import TestCase

from locations.services import LocationServices
from locations.models import Location
from locations.providers.google_location_services import GoogleLocationServices


class TestLocationServices(TestCase):

    def setUp(self):
        pass

    def test_location_create(self):

        created, location = LocationServices.create(
            lat=1.0,
            lon=1.0,
            address_line="No mans land",
            )

        self.assertTrue(created)
        self.assertIsInstance(location, Location)


class TesteGoogleLocationServices(TestCase):
    pass
    # TODO: write this.
